#!/bin/bash
# create ossutil command

# check if exists bin. create it first if not exists
if [[ ! -d ../../../bin ]]; then
    mkdir ../../../bin
fi
# checkout workspace
# swith the runnable dir
cd ../../../bin
# e.g. Linux,get the ossutil 
wget http://gosspublic.alicdn.com/ossutil/1.6.19/ossutil64
# mac
# curl -o ossutil64 http://gosspublic.alicdn.com/ossutil/1.6.19/ossutilmac64
# change the privilege
chmod 755 ossutil64
# cp the oss config
cp ../config/.ossutilconfig .ossutilconfig