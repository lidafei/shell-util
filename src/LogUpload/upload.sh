#!/bin/bash
# split and upload logs to Aliyun OSS

# get config as array
CHUNK_NUM="50000"
UPLOAD_DATE=`date +%Y%m%d`
OSS_BUCKET=""

echo "now is "$(date +%Y-%m-%d\ %H:%M:%S)
echo "========== start =========="

# slipce string as array
function __sliceString()
{
    echo ${1//,/ } 
}

# slipce file as chunk by line
function __handleFile()
{
    local _FILE_PATH=$1
    local _FILE_NAME=${_FILE_PATH##*/}
    local _TMP_PATH="/tmp/ossupload"
    local _GROUP=$2
    local _TimeFlag=$3

    if [[ $_TimeFlag == "1" ]];then
      local _TMP_FILE_PATH=$_TMP_PATH"/"$_FILE_NAME
      local _upload_file_name="oss://"$OSS_BUCKET"/"$_GROUP"/"$_FILE_NAME
    else
      local _TMP_FILE_PATH=$_TMP_PATH"/"$_FILE_NAME"."$UPLOAD_DATE
      local _upload_file_name="oss://"$OSS_BUCKET"/"$_GROUP"/"$_FILE_NAME"."$UPLOAD_DATE
    fi

    # check if exists
    if [[ ! -f "$_FILE_PATH" ]];then
        echo $_FILE_PATH" not found"
        return
    fi

    if [[ ! -r "$_FILE_PATH" ]];then
        echo $_FILE_PATH" do not have permission to read"
        return
    fi

    local _FILE_SIZE=`cat $_FILE_PATH|wc -l`
    declare -i _FILE_SIZE

    if [[ $_FILE_SIZE -le 0 ]];then
        echo $_FILE_PATH" has no content"
        return
    fi

    # slice file
    if [[ ! -d $_TMP_PATH ]];then
        mkdir $_TMP_PATH
    fi

    mv $_FILE_PATH $_TMP_FILE_PATH

    if [[ $_FILE_SIZE -le $CHUNK_NUM ]];then
        echo "uploading "$_TMP_FILE_PATH" ..."
        res=`./ossutil64 --config-file .ossutilconfig appendfromfile $_TMP_FILE_PATH $_upload_file_name`
        echo $res
        if [[ $res == *"(100.00%)"* ]]; then
              echo $_TMP_FILE_PATH" uploaded"
              rm -f $_TMP_FILE_PATH
              echo $_TMP_FILE_PATH" removed"
        fi

        return
    fi

    split -a 2 -l $CHUNK_NUM $_FILE_PATH $_TMP_FILE_PATH

    rm -f $_TMP_FILE_PATH

    local _WAIT_FILES=$(ls $_TMP_PATH"/"$_FILE_NAME*)

    for wait_file in $_WAIT_FILES
    do
        echo "uploading "$wait_file" ..."
        res=`./ossutil64 --config-file .ossutilconfig appendfromfile $wait_file $_upload_file_name`
        echo $res
        if [[ $res == *"(100.00%)"* ]]; then
            echo $wait_file" uploaded1"
            rm -f $wait_file
            echo $wait_file" removed"
        fi

    done
}

# handle configuration file,loop 
function __handleByConfig()
{
    local _FILE_NAME=$1
    local _TAG=""
    local _GROUP=""
    local _GroupPath=""


    for line in `cat $_FILE_NAME`
    do
        if [[ $line = "[Config]" ]];then
            _TAG="config"
        elif [[ $line = "[Group]" ]];then
            _TAG="group"
        elif [[ $line = "[Dir]" ]];then
            _TAG="dir"
        elif [[ $_TAG = "config" ]] && [[ `echo $line | cut -d \= -f 1` = "chunk" ]];then
            CHUNK_NUM=`echo $line | cut -d \= -f 2`
            echo "set the chunk number from configuration file as "$CHUNK_NUM
        elif [[ $_TAG = "config" ]] && [[ `echo $line | cut -d \= -f 1` = "bucket" ]];then
            OSS_BUCKET=`echo $line | cut -d \= -f 2`
            echo "set the bucket from configuration file as "$OSS_BUCKET
        elif [[ $_TAG = "group" ]];then
            _GROUP=`echo $line | cut -d \= -f 1`
            for file in $(__sliceString `echo $line | cut -d \= -f 2`)
            do
                __handleFile $file $_GROUP
            done
        elif [[ $_TAG = "dir" ]];then
          _GROUP=`echo $line | cut -d \= -f 1`
          _GroupPath=`echo $line | cut -d \= -f 2`
          for filePath in ${_GroupPath}/${_GROUP}*
          do
            if [[ ! -L $filePath ]];then
              if [[ ! $filePath == *$UPLOAD_DATE ]]; then
                __handleFile $filePath $_GROUP 1
              fi
            fi
          done

        fi
    done
}

__handleByConfig "../config/env.ini"

echo "==========  end  =========="