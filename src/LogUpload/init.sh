#!/bin/bash

if [[ ! -d ../../bin ]]; then
    mkdir ../../bin
fi

CURRENT_PATH=`pwd`

ln -s $CURRENT_PATH"/upload.sh" ../../bin/upload

chmod a+x ../../bin/upload