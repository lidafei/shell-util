# Shell Util

#### 介绍
shell小工具

#### 软件架构
Linux Bash


#### 安装教程

1.  cd 进入src/Dependency/Oss路径，根据操作系统Linux/Mac选择合适的命令
2.  cp config/ossutilconfig config/.ossutilconfig，修改配置为自己的oss配置
3.  执行./init.sh，生成命令行工具ossutil64
4.  cp config/env.ini.tpl config/env.ini，按照实际情况修改配置
5.  cd 进入src/LogUpload路径，执行./init.sh，在bin目录下生成软连接文件upload

#### 使用说明

1.  执行方式：cd $PATH_OF_PROJECT/bin;./upload
2.  目前执行upload存在路径依赖，必须cd到bin目录下，有需要可以查看upload.sh调整相对路径依赖环境变量
